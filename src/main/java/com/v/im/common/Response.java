package com.v.im.common;


import org.springframework.http.HttpStatus;

/**
 * 注册返回信息
 * TODO 调整前端对于状态码的判断，使用HTTP标准状态码
 * @author 乐天
 * @since 2018-10-07
 */
public class Response<T> {

    private static final String SUCCESS = "0";

    private static final String ERROR = "1";

    /**
     * TODO 将String resultCode 改为 int code
     */
    private String resultCode;

    private String message;

    /**
     * 请求成功
     */
    public static Response success() {
        return new Response();
    }

    /**
     * 请求出错(通用错误)
     */
    public static Response error(String msg) {
        return new Response(ERROR, msg);
    }

    private Response() {
        this.resultCode = SUCCESS;
        this.message = "success";
    }

    private Response(String code, String message) {
        this.resultCode = code;
        this.message = message;
    }

//    private static final HttpStatus SUCCESS = HttpStatus.OK;

//    private static final HttpStatus BAD_REQUEST = HttpStatus.BAD_REQUEST;

//    private int code;

//    private T data;

//    /**
//     * 请求成功
//     */
//    public static Response success() {
//        return new Response();
//    }
//
//    /**
//     * 请求成功
//     */
//    public static <T> Response<T> success(T t) {
//        return new Response<T>(SUCCESS.value(), SUCCESS.getReasonPhrase(), t);
//    }
//
//    /**
//     * 请求出错(通用错误)
//     */
//    public static Response error() {
//        return new Response(BAD_REQUEST.value(), BAD_REQUEST.getReasonPhrase());
//    }
//
//    /**
//     * 请求出错(通用错误)
//     */
//    public static Response error(String msg) {
//        return new Response(BAD_REQUEST.value(), msg);
//    }
//
//    private Response() {
//        this.code = SUCCESS.value();
//        this.message = "success";
//    }
//
//
//    private Response(int code, String message) {
//        this.code = code;
//        this.message = message;
//    }
//
//
//    private Response(int code, String message, T data) {
//        this.code = code;
//        this.message = message;
//        this.data = data;
//    }
//
//    @Override
//    public String toString() {
//        return "Response{" +
//                "code=" + code +
//                ", message='" + message + '\'' +
//                ", data=" + data +
//                '}';
//    }
}
