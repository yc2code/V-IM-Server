package com.v.im.controller;

import com.v.im.common.Response;
import com.v.im.entity.ImUser;
import com.v.im.service.IImUserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 注册
 *
 * @author 乐天
 * @since 2018-10-07
 */
@RestController
public class RegisterController {

    private static final String DEFAULT_AVATAR = "/img/default-user.png";

    private static final String BCRYPT = "{bcrypt}";

    @Resource
    @Qualifier(value = "imUserService")
    private IImUserService imUserService;

    /**
     * 用户注册
     *
     * @param name     用户名
     * @param password 密码
     * @param phone    手机
     * @return 结果
     */
    @RequestMapping(value = "register", method = RequestMethod.POST)
    public Response register(String name, String password, String phone) {
        if (imUserService.getByLoginName(phone) != null) {
            return Response.error("手机号码重复");
        } else {
            try {
                ImUser imUser = new ImUser();
                String finalSecret = BCRYPT + new BCryptPasswordEncoder().encode(password.trim());
                imUser.setPassword(finalSecret);
                imUser.setLoginName(phone);
                imUser.setMobile(phone);
                imUser.setName(name);
                imUser.setAvatar(DEFAULT_AVATAR);
                imUserService.registerUser(imUser);
                return Response.success();
            } catch (Exception e) {
                return Response.error("保存用户失败");
            }
        }
    }
}
