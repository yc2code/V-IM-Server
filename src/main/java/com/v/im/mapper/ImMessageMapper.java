package com.v.im.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.v.im.entity.ImMessage;

/**
 * Mapper 接口
 *
 * @author 乐天
 * @since 2018-10-08
 */
public interface ImMessageMapper extends BaseMapper<ImMessage> {

}
